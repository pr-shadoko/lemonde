# README #

This app is a RSS feed reader for the website www.lemonde.fr

## Features ##

* List/Detail view of articles

* Social networks sharing

* Pull-to-refresh on article list

* Loading spinner

* Smartphone & tablet compatibility

* Material design (cards, floating action button, collapsing toolbar)

## Download ##

The apk can be downloaded from the project's *Downloads* section, [here](https://bitbucket.org/pr-shadoko/lemonde/downloads)