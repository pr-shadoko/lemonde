package fr.trichard.lemonde.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;

import java.io.ByteArrayInputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import fr.trichard.lemonde.model.db.DatabaseContract;
import fr.trichard.lemonde.model.db.DatabaseHelper;
import fr.trichard.lemonde.util.TaskCompletionListener;

/**
 *
 */
public class ArticleCursorLoader extends AsyncTask<Long, Void, Article> {
    private Context context;
    private TaskCompletionListener<Article> completionListener;

    public ArticleCursorLoader(Context context, TaskCompletionListener<Article> completionListener) {
        this.context = context.getApplicationContext();
        this.completionListener = completionListener;
    }

    @Override
    protected Article doInBackground(Long... id) {
        if(id[0] == -1L) {
            return null;
        }
        SQLiteDatabase db = new DatabaseHelper(context).getReadableDatabase();
        Cursor result = db.query(
        DatabaseContract.TABLE_ARTICLE,
        new String[] {
        DatabaseContract.ARTICLE_ID,
        DatabaseContract.ARTICLE_TITLE,
        DatabaseContract.ARTICLE_PUBLICATION_DATE,
        DatabaseContract.ARTICLE_BANNER,
        DatabaseContract.ARTICLE_SHORT_CONTENT
        },
        DatabaseContract.ARTICLE_ID + " = ?",
        new String[] {String.valueOf(id[0])},
        null, null, null
        );

        result.moveToNext();
        int idIndex = result.getColumnIndexOrThrow(DatabaseContract.ARTICLE_ID);
        int titleIndex = result.getColumnIndexOrThrow(DatabaseContract.ARTICLE_TITLE);
        int dateIndex = result.getColumnIndexOrThrow(DatabaseContract.ARTICLE_PUBLICATION_DATE);
        int descriptionIndex = result.getColumnIndexOrThrow(DatabaseContract.ARTICLE_SHORT_CONTENT);
        int banner_index = result.getColumnIndexOrThrow(DatabaseContract.ARTICLE_BANNER);

        Article article = new Article();
        article.setId(result.getLong(idIndex));
        article.setTitle(result.getString(titleIndex));
        try {
            article.setPublicationDate(DatabaseContract.DATE_FORMAT.parse(result.getString(dateIndex)));
        } catch(ParseException e) {
            e.printStackTrace();
        }
        article.setDescription(result.getString(descriptionIndex));
        article.setPicture(Drawable.createFromStream(new ByteArrayInputStream(result.getBlob(banner_index)), null));
        result.close();
        db.close();
        return article;
    }

    @Override
    protected void onPostExecute(Article article) {
        if(completionListener != null) {
            completionListener.onCompletion(article);
        }
    }
}