package fr.trichard.lemonde.model;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import fr.trichard.lemonde.model.db.DatabaseContract;
import fr.trichard.lemonde.model.db.DatabaseHelper;

/**
 *
 */
public class FeedLoaderModule {
    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat inputDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z");
    private DatabaseHelper dbHelper;

    public FeedLoaderModule(DatabaseHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    public ArrayList<ArticleItem> loadList(String url) {
        ArrayList<ArticleItem> items = null;
        try {
            InputStream inputStream = openConnection(url);
            try {
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(inputStream, null);
                items = readFeed(parser);
            } finally {
                //noinspection ThrowFromFinallyBlock
                inputStream.close();
            }
        } catch(IOException | XmlPullParserException e) {
            e.printStackTrace();
        }
        return items;
    }

    private InputStream openConnection(String url) throws IOException {
        URL feedUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) feedUrl.openConnection();
        connection.setReadTimeout(10 * 1000);
        connection.setConnectTimeout(10 * 1000);
        connection.setRequestMethod("GET");
        connection.setDoInput(true);
        connection.connect();
        int response = connection.getResponseCode();
        if(response != 200) {
            throw new IOException("Invalid server response.");
        }
        return connection.getInputStream();
    }

    private ArrayList<ArticleItem> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<ArticleItem> items = new ArrayList<>();
        while(parser.next() != XmlPullParser.END_DOCUMENT) {
            if(parser.getEventType() == XmlPullParser.START_TAG) {
                String tagName = parser.getName();
                switch(tagName) {
                    case "rss":
                    case "channel":
                        // not skipping these to go down to item tags level
                        break;
                    case "item":
                        // parsing item tags
                        items.add(readItem(parser));
                        break;
                    default:
                        // skipping any useless tag
                        skip(parser);
                        break;
                }
            }
        }
        return items;
    }

    private ArticleItem readItem(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArticleItem articleItem = new ArticleItem();
        parser.require(XmlPullParser.START_TAG, null, "item");
        while(parser.next() != XmlPullParser.END_TAG) {
            if(parser.getEventType() == XmlPullParser.START_TAG) {
                String tagName = parser.getName();
                switch(tagName) {
                    case "title":
                        articleItem.title = readSimpleValue(parser, tagName);
                        break;
                    case "link":
                        String link = readSimpleValue(parser, tagName);
                        articleItem.link = link.replaceFirst("www", "mobile");
                        break;
                    case "description":
                        articleItem.description = stripTags(readSimpleValue(parser, tagName));
                        break;
                    case "pubDate":
                        String pubDate = readSimpleValue(parser, tagName);
                        try {
                            articleItem.pubDate = DatabaseContract.DATE_FORMAT.format(inputDateFormat.parse(pubDate));
                        } catch(ParseException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "guid":
                        String guid = readSimpleValue(parser, tagName);
                        articleItem.guid = Integer.parseInt(guid.replaceAll("[^0-9]", ""));
                        break;
                    case "enclosure":
                        articleItem.imageUrl = readEnclosure(parser);
                        break;
                    default:
                        skip(parser);
                        break;
                }
            }
        }
        return articleItem;
    }

    private String readEnclosure(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, "enclosure");
        String value = parser.getAttributeValue(null, "url");
        parser.nextTag();
        parser.require(XmlPullParser.END_TAG, null, "enclosure");
        return value;
    }

    private String readSimpleValue(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, null, tag);
        String value = readPlainText(parser);
        parser.require(XmlPullParser.END_TAG, null, tag);
        return value;
    }

    private String readPlainText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if(parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if(parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while(depth != 0) {
            switch(parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }

    public boolean processItem(ArticleItem item) {
        item.imageData = downloadImage(item.imageUrl);
        return saveItem(item);
    }

    private byte[] downloadImage(String url) {
        try {
            URL imageUrl = new URL(url);
            InputStream inputStream = new BufferedInputStream(imageUrl.openStream());
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n;
            while(-1 != (n = inputStream.read(buf))) {
                outputStream.write(buf, 0, n);
            }
            outputStream.close();
            inputStream.close();
            return outputStream.toByteArray();
        } catch(IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean saveItem(ArticleItem item) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if(!isArticleInDatabase(db, item)) {
            long id = db.insert(DatabaseContract.TABLE_ARTICLE, null, item.toContentValues());
            if(id == -1) {
                Log.d("DEMO", "article insertion failed");
                return false;
            } else {
                Log.d("DEMO", "article insertion succeeded");
            }
        }
        db.close();
        return true;
    }

    private boolean isArticleInDatabase(SQLiteDatabase db, ArticleItem item) {
        String query = SQLiteQueryBuilder.buildQueryString(
        false,
        DatabaseContract.TABLE_ARTICLE,
        new String[] {DatabaseContract.ARTICLE_ID},
        DatabaseContract.ARTICLE_ID + "=" + String.valueOf(item.guid),
        null, null, null, null
        );
        SQLiteStatement statement = db.compileStatement(query);
        try {
            statement.simpleQueryForLong();
        } catch(SQLiteDoneException e) {
            //thrown if the article is not in the database and needs to be fetched
            return false;
        }
        //if the article is already loaded in database, we should not reload it
        return true;
    }

    /**
     * Removes any html tag.
     *
     * @param dirtyString the string to clean
     * @return the cleaned string
     */
    private String stripTags(String dirtyString) {
        return dirtyString.replaceAll("</?\\w+(?: \\w+(?:=[\"'][^\"']*[\"'])?)* ?/?>", "");
    }

    public class ArticleItem {
        int guid;
        String title;
        String link;
        String pubDate;
        String description;
        String imageUrl;
        byte[] imageData;

        public ContentValues toContentValues() {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DatabaseContract.ARTICLE_ID, guid);
            contentValues.put(DatabaseContract.ARTICLE_TITLE, title);
            contentValues.put(DatabaseContract.ARTICLE_LINK, link);
            contentValues.put(DatabaseContract.ARTICLE_PUBLICATION_DATE, pubDate);
            contentValues.put(DatabaseContract.ARTICLE_SHORT_CONTENT, description);
            contentValues.put(DatabaseContract.ARTICLE_BANNER, imageData);
            return contentValues;
        }
    }
}
