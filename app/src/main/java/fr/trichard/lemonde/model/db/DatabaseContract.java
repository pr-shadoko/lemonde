package fr.trichard.lemonde.model.db;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 *
 */
public abstract class DatabaseContract {
    public static final String TABLE_ARTICLE = "article";
    public static final String ARTICLE_ID = "_id";
    public static final String ARTICLE_TITLE = "title";
    public static final String ARTICLE_LINK = "link";
    public static final String ARTICLE_PUBLICATION_DATE = "publication_date";
    public static final String ARTICLE_SHORT_CONTENT = "short_content";
    public static final String ARTICLE_BANNER = "banner";
    @SuppressLint("SimpleDateFormat")
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
}
