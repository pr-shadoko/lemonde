package fr.trichard.lemonde.model;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;

import fr.trichard.lemonde.model.db.DatabaseHelper;

/**
 *
 */

public class FeedLoaderService extends Service implements FeedLoaderHandler.FeedLoaderCallback {
    /* ***** Intent.extras keys ***** */
    public static final String EXTRA_RESULT_RECEIVER = "result_receiver";
    public static final String EXTRA_FEED_URL = "feed_url";

    /* ***** members ***** */
    ResultReceiver resultReceiver;
    private Thread t;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        resultReceiver = (ResultReceiver) intent.getExtras().get(EXTRA_RESULT_RECEIVER);
        if(t == null) {
            t = new LoaderThread(this, intent.getStringExtra(EXTRA_FEED_URL));
            t.start();
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onProgress(int progress, int max) {
        //TODO
    }

    @Override
    public void onSuccess() {
        resultReceiver.send(Activity.RESULT_OK, null);
        stopSelf();
    }

    @Override
    public void onFailure() {
        resultReceiver.send(Activity.RESULT_CANCELED, null);
        stopSelf();
    }

    @Override
    public void onDestroy() {
        if(t.isAlive()) {
            //TODO: stop thread
        }
        super.onDestroy();
    }

    class LoaderThread extends Thread {
        private FeedLoaderHandler handler;
        private FeedLoaderHandler.FeedLoaderCallback callback;
        private String url;

        public LoaderThread(FeedLoaderHandler.FeedLoaderCallback callback, String url) {
            this.callback = callback;
            this.url = url;
        }

        @Override
        public void run() {
            Looper.prepare();
            if(handler == null) {
                handler = new FeedLoaderHandler(callback, new DatabaseHelper(getApplicationContext()));
            }
            Message msg = handler.obtainMessage(FeedLoaderHandler.MSG_STARTED);
            Bundle data = new Bundle();
            data.putString(FeedLoaderHandler.ARG_FEED_URL, url);
            msg.setData(data);
            handler.sendMessage(msg);

            Looper.loop();
        }
    }
}