package fr.trichard.lemonde.model.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
    private final static String DB_NAME = "demo.db3";
    private final static int DB_VERSION = 1;
    private static DatabaseHelper instance = null;

    public DatabaseHelper(Context context) {
        super(context.getApplicationContext(), DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(
        "CREATE TABLE " + DatabaseContract.TABLE_ARTICLE + " ( " +
        DatabaseContract.ARTICLE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
        DatabaseContract.ARTICLE_TITLE + " TEXT NOT NULL, " +
        DatabaseContract.ARTICLE_LINK + " TEXT NOT NULL, " +
        DatabaseContract.ARTICLE_PUBLICATION_DATE + " TEXT NOT NULL, " +
        DatabaseContract.ARTICLE_SHORT_CONTENT + " TEXT NOT NULL, " +
        DatabaseContract.ARTICLE_BANNER + " BLOB NOT NULL )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.d("onUpgrade", "Updating database from version " + oldVersion + " to version " + newVersion);
        if(oldVersion < newVersion) {
            switch(oldVersion) {
//                case 1:
//                  database.execSQL("<upgrade operation>");
//                // No break
//                case 2:
//                  database.execSQL("<upgrade operation>");
//                // No break
                default:
                    break;
            }
        }
    }
}