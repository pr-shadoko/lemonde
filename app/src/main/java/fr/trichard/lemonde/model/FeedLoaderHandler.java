package fr.trichard.lemonde.model;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import fr.trichard.lemonde.model.db.DatabaseHelper;

/**
 *
 */

public class FeedLoaderHandler extends Handler {
    /* ***** Message.what constants ***** */
    /**
     * Must be accompanied by true/false in message.object for success/failure
     */
    public static final int MSG_ENDED = -1;
    /**
     * Must be accompanied by the feed url.
     *
     * @see FeedLoaderHandler#ARG_FEED_URL
     */
    public static final int MSG_STARTED = 0;
    /**
     * Must be accompanied by the item list.
     *
     * @see FeedLoaderHandler#ARG_ITEMS
     */
    public static final int MSG_LIST_LOADED = 1;

    /* ***** Message.data keys ***** */
    public static final String ARG_FEED_URL = "feed_url";
    public static final String ARG_ITEMS = "items";

    /* ***** members ***** */
    private FeedLoaderCallback callback;
    private FeedLoaderModule module;

    public FeedLoaderHandler(@NonNull FeedLoaderCallback feedLoaderCallback, @NonNull DatabaseHelper databaseHelper) {
        this.callback = feedLoaderCallback;
        this.module = new FeedLoaderModule(databaseHelper);
    }

    @Override
    public void handleMessage(Message message) {
        switch(message.what) {
            case MSG_STARTED: {
                ArrayList<FeedLoaderModule.ArticleItem> items = module.loadList(message.getData().getString(ARG_FEED_URL));
                Message nextMessage = obtainMessage(MSG_LIST_LOADED);
                Bundle data = new Bundle();
                data.putSerializable(ARG_ITEMS, items);
                nextMessage.setData(data);
                sendMessage(nextMessage);
                break;
            }
            case MSG_LIST_LOADED: {
                //noinspection unchecked
                ArrayList<FeedLoaderModule.ArticleItem> items = (ArrayList<FeedLoaderModule.ArticleItem>) message.getData().getSerializable(ARG_ITEMS);
                if(items != null) {
                    for(int progress = 0; progress < items.size(); progress++) {
                        if(!module.processItem(items.get(progress))) {
                            sendMessage(obtainMessage(MSG_ENDED, false));
                        }
                        callback.onProgress(progress, items.size());
                    }
                }

                sendMessage(obtainMessage(MSG_ENDED, true));
                break;
            }
            case MSG_ENDED: {
                if((Boolean) message.obj) {
                    callback.onSuccess();
                } else {
                    callback.onFailure();
                }
                break;
            }
        }
    }

    public interface FeedLoaderCallback {
        void onProgress(int progress, int max);

        void onSuccess();

        void onFailure();
    }
}
