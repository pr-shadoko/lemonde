package fr.trichard.lemonde.model;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import fr.trichard.lemonde.model.db.DatabaseContract;
import fr.trichard.lemonde.model.db.DatabaseHelper;
import fr.trichard.lemonde.util.TaskCompletionListener;

/**
 *
 */

public class ArticleListCursorLoader extends AsyncTask<Void, Void, Cursor> {
    private Context context;
    private TaskCompletionListener<Cursor> completionListener;

    public ArticleListCursorLoader(Context context, TaskCompletionListener<Cursor> completionListener) {
        this.context = context.getApplicationContext();
        this.completionListener = completionListener;
    }

    @Override
    protected Cursor doInBackground(Void... noParams) {
        SQLiteDatabase db = new DatabaseHelper(context).getReadableDatabase();
        return db.query(
        DatabaseContract.TABLE_ARTICLE,
        new String[] {
        DatabaseContract.ARTICLE_ID,
        DatabaseContract.ARTICLE_TITLE,
        DatabaseContract.ARTICLE_BANNER
        },
        null, null, null, null,
        DatabaseContract.ARTICLE_PUBLICATION_DATE + " DESC"
        );
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        if(completionListener != null) {
            completionListener.onCompletion(cursor);
        }
    }
}
