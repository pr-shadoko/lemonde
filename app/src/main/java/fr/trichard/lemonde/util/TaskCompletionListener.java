package fr.trichard.lemonde.util;

/**
 *
 */

public interface TaskCompletionListener<T> {
    void onCompletion(T result);
}
