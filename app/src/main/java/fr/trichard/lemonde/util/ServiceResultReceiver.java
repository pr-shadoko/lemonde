package fr.trichard.lemonde.util;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

/**
 *
 */
public class ServiceResultReceiver extends ResultReceiver {
    private Callback callback;

    // Constructor takes a handler
    public ServiceResultReceiver(Handler handler, Callback callback) {
        super(handler);
        this.callback = callback;
    }

    // Delegate method which passes the result to the receiver if the receiver has been assigned
    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        callback.onReceiveResult(resultCode, resultData);
    }

    public interface Callback {
        void onReceiveResult(int resultCode, Bundle resultData);
    }
}
