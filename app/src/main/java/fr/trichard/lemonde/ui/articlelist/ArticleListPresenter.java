package fr.trichard.lemonde.ui.articlelist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import fr.trichard.lemonde.R;
import fr.trichard.lemonde.model.FeedLoaderService;
import fr.trichard.lemonde.model.ArticleListCursorLoader;
import fr.trichard.lemonde.util.ConnectivityChecker;
import fr.trichard.lemonde.util.TaskCompletionListener;
import fr.trichard.lemonde.util.ServiceResultReceiver;

/**
 *
 */

public class ArticleListPresenter extends MvpBasePresenter<ArticleListView> implements ServiceResultReceiver.Callback, TaskCompletionListener<Cursor> {
    private static final String FEED_URL = "http://www.lemonde.fr/rss/une.xml";
    private ServiceResultReceiver serviceResultReceiver;
    private ArticleListCursorLoader articleListCursorLoader;
    private Context context;

    public ArticleListPresenter(Context context) {
        this.context = context.getApplicationContext();
        serviceResultReceiver = new ServiceResultReceiver(new Handler(), this);
    }

    public void loadFeed() {
        if(ConnectivityChecker.isConnectionAvailable(context)) {
            Intent feedLoaderServiceIntent = new Intent(context, FeedLoaderService.class);
            feedLoaderServiceIntent.putExtra(FeedLoaderService.EXTRA_RESULT_RECEIVER, serviceResultReceiver);
            feedLoaderServiceIntent.putExtra(FeedLoaderService.EXTRA_FEED_URL, FEED_URL);
            context.startService(feedLoaderServiceIntent);
        } else {
            if(isViewAttached()) {
                ArticleListView v = getView();
                //noinspection ConstantConditions
                v.onError(R.string.error_connection_msg);
            }
        }
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle resultData) {
        if(resultCode == Activity.RESULT_OK) {
            loadArticles();
        }
    }

    public void loadArticles() {
        if(articleListCursorLoader != null && articleListCursorLoader.getStatus() == AsyncTask.Status.RUNNING) {
            return;
        }
        articleListCursorLoader = new ArticleListCursorLoader(context, this);
        articleListCursorLoader.execute();
    }

    @Override
    public void onCompletion(Cursor cursor) {
        if(isViewAttached()) {
            //noinspection ConstantConditions
            getView().onData(cursor);
        }
    }
}