package fr.trichard.lemonde.ui.articledetail;

import android.database.Cursor;
import android.support.annotation.StringRes;

import com.hannesdorfmann.mosby.mvp.MvpView;

import fr.trichard.lemonde.model.Article;

/**
 *
 */

public interface ArticleDetailView extends MvpView {
    void onError(String errorMessage);

    void onError(@StringRes int errorMessageResId);

    void onData(Article article);
}
