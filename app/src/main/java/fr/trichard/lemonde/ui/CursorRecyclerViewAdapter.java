package fr.trichard.lemonde.ui;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.ByteArrayInputStream;

import fr.trichard.lemonde.R;
import fr.trichard.lemonde.model.db.DatabaseContract;

/**
 *
 */
public class CursorRecyclerViewAdapter extends RecyclerView.Adapter<ArticleViewHolder> {
    private CursorAdapter cursorAdapter;
    private ArticleClickListener articleClickListener;
    private long selectedItemId = RecyclerView.NO_ID;
    private ArticleViewHolder selectedItem;

    public CursorRecyclerViewAdapter() {
        setHasStableIds(true);
        this.cursorAdapter = new CursorAdapter(null, null, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER) {
            @Override
            public View newView(Context context, Cursor cursor, ViewGroup parent) {
                return LayoutInflater.from(parent.getContext()).inflate(R.layout.article_list_content, parent, false);
            }

            @Override
            public void bindView(View view, Context context, Cursor cursor) {
                throw new UnsupportedOperationException("CursorRecyclerViewAdapter : Binding view is not meant to be done by embedded CursorAdapter.");
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
                CursorRecyclerViewAdapter.this.notifyDataSetChanged();
            }
        };
    }

    public void setArticleClickListener(ArticleClickListener articleClickListener) {
        this.articleClickListener = articleClickListener;
    }

    public void changeCursor(Cursor cursor) {
        cursorAdapter.changeCursor(cursor);
    }

    public Cursor swapCursor(Cursor cursor) {
        return cursorAdapter.swapCursor(cursor);
    }

    @Override
    public ArticleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = cursorAdapter.newView(null, cursorAdapter.getCursor(), parent);
        return new ArticleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ArticleViewHolder articleViewHolder, int position) {
        Cursor cursor = cursorAdapter.getCursor();
        if(cursor == null) {
            throw new IllegalArgumentException("Unable to bind data form a null cursor to view.");
        }
        if(cursor.isAfterLast()) {
            throw new IllegalArgumentException("Unable to bind data form the end of cursor to view.");
        }
        cursor.moveToPosition(position);
        Drawable banner = Drawable.createFromStream(new ByteArrayInputStream(cursor.getBlob(cursor.getColumnIndexOrThrow(DatabaseContract.ARTICLE_BANNER))), null);
        String title = cursor.getString(cursor.getColumnIndexOrThrow(DatabaseContract.ARTICLE_TITLE));

        if(selectedItemId != RecyclerView.NO_ID && selectedItemId == articleViewHolder.getItemId()) {
            selectedItem = articleViewHolder;
            selectedItemId = RecyclerView.NO_ID;
        }

        articleViewHolder.itemView.setSelected(selectedItem != null && selectedItem.getAdapterPosition() == position);
        articleViewHolder.picture.setImageDrawable(banner);
        articleViewHolder.title.setText(title);
        articleViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedItem = articleViewHolder;
                if(articleClickListener != null) {
                    articleClickListener.onArticleClicked(articleViewHolder.getItemId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return cursorAdapter.getCount();
    }

    @Override
    public long getItemId(int position) {
        return cursorAdapter.getItemId(position);
    }

    public long getSelectedItemId() {
        if(selectedItemId != RecyclerView.NO_ID) {
            return selectedItemId;
        }
        if(selectedItem != null) {
            return selectedItem.getItemId();
        }
        return RecyclerView.NO_ID;
    }

    public void setSelectedItemId(long itemId) {
        selectedItemId = itemId;
    }

    public interface ArticleClickListener {
        void onArticleClicked(long articleId);
    }
}