package fr.trichard.lemonde.ui;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.view.View;

import fr.trichard.lemonde.R;

/**
 *
 */
public class ShareFloatingActionButton extends FloatingActionButton implements View.OnClickListener {
    private String url;

    public ShareFloatingActionButton(Context context) {
        this(context, null);
    }

    public ShareFloatingActionButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ShareFloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setOnClickListener(this);
    }

    public void setSharedData(String url) {
        this.url = url;
    }

    @Override
    public void onClick(View v) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, url);
        sendIntent.setType("text/plain");
        getContext().startActivity(Intent.createChooser(sendIntent, getContext().getString(R.string.share_chooser_title)));
    }
}
