package fr.trichard.lemonde.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.trichard.lemonde.R;

/**
 *
 */
public class ArticleViewHolder extends RecyclerView.ViewHolder {
    final ImageView picture;
    final TextView title;

    ArticleViewHolder(View view) {
        super(view);
        this.picture = (ImageView) view.findViewById(R.id.banner);
        this.title = (TextView) view.findViewById(R.id.title);
    }

    @Override
    public String toString() {
        return super.toString() + " '" + title.getText() + "'";
    }
}
