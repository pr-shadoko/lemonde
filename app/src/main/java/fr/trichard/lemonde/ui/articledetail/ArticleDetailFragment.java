package fr.trichard.lemonde.ui.articledetail;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hannesdorfmann.mosby.mvp.MvpFragment;

import java.text.DateFormat;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.trichard.lemonde.R;
import fr.trichard.lemonde.model.Article;
import fr.trichard.lemonde.ui.articlelist.ArticleListActivity;

/**
 * A fragment representing a single Article detail screen.
 * This fragment is either contained in a {@link ArticleListActivity}
 * in two-pane mode (on tablets) or a {@link ArticleDetailActivity}
 * on handsets.
 */
public class ArticleDetailFragment extends MvpFragment<ArticleDetailView, ArticleDetailPresenter> implements ArticleDetailView {
    public static final String ARG_ITEM_ID = "item_id";
    public static final Long INVALID_ID = -1L;

    @BindView(R.id.text_detail_description)
    TextView descriptionText;
    @BindView(R.id.text_detail_date)
    TextView dateText;

    private long articleId = INVALID_ID;

    @NonNull
    @Override
    public ArticleDetailPresenter createPresenter() {
        return new ArticleDetailPresenter(getContext());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle init = savedInstanceState != null ? savedInstanceState : getArguments();
        if(init != null) {
            if(init.containsKey(ARG_ITEM_ID)) {
                articleId = init.getLong(ARG_ITEM_ID, INVALID_ID);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(ARG_ITEM_ID, articleId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.article_detail, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(presenter != null) {
            presenter.loadArticle(articleId);
        }
    }

    private void showMessage(String message) {
        View view = getView();
        if(view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onError(String errorMessage) {
        showMessage(errorMessage);
    }

    @Override
    public void onError(@StringRes int errorMessageResId) {
        onError(getString(errorMessageResId));
    }

    @Override
    public void onData(Article article) {
        if(article == null) {
            showMessage("Unable to load the article to display.");
        } else {
            getActivity().setTitle(article.getTitle());
            dateText.setText(DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault()).format(article.getPublicationDate()));
            descriptionText.setText(article.getDescription());
            ImageView articlePicture = null;
            View view = getView();
            if(view != null) {
                articlePicture = (ImageView) view.findViewById(R.id.image_detail_picture);
            }
            if(articlePicture == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                articlePicture = (ImageView) getActivity().findViewById(R.id.toolbar_banner);
            }
            if(articlePicture != null) {
                articlePicture.setImageDrawable(article.getPicture());
            }
        }
    }
}
