package fr.trichard.lemonde.ui.articlelist;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewSwitcher;

import com.hannesdorfmann.mosby.mvp.MvpFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.trichard.lemonde.ui.CursorRecyclerViewAdapter;
import fr.trichard.lemonde.R;

/**
 *
 */
public class ArticleListFragment extends MvpFragment<ArticleListView, ArticleListPresenter> implements ArticleListView, SwipeRefreshLayout.OnRefreshListener {
    private static final String KEY_SELECTED_ITEM = "keySelectedItem";

    @BindView(R.id.swipe_refresh)
    protected SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.article_list)
    protected RecyclerView recyclerView;
    @BindView(R.id.switcher)
    protected ViewSwitcher emptyViewSwitcher;
    private CursorRecyclerViewAdapter adapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        adapter = new CursorRecyclerViewAdapter();
        if(context instanceof CursorRecyclerViewAdapter.ArticleClickListener) {
            adapter.setArticleClickListener((CursorRecyclerViewAdapter.ArticleClickListener) context);
        } else {
            throw new ClassCastException("Current Context cannot be cast to CursorRecyclerViewAdapter.ArticleClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        adapter.setArticleClickListener(null);
    }

    @NonNull
    @Override
    public ArticleListPresenter createPresenter() {
        return new ArticleListPresenter(getContext());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle init = savedInstanceState != null ? savedInstanceState : getArguments();
        if(init != null) {
            adapter.setSelectedItemId(init.getLong(KEY_SELECTED_ITEM));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.article_list, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int firstPos = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(firstPos > 0) {
                    swipeRefreshLayout.setEnabled(false);
                } else {
                    swipeRefreshLayout.setEnabled(true);
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        if(presenter != null) {
            presenter.loadFeed();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(presenter != null) {
            presenter.loadArticles();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(KEY_SELECTED_ITEM, getSelectedArticleId());
    }

    public long getSelectedArticleId() {
        return adapter.getSelectedItemId();
    }

    private void showMessage(String message) {
        View view = getView();
        if(view != null) {
            Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onError(@StringRes int errorMessageResId) {
        onError(getString(errorMessageResId));
    }

    @Override
    public void onError(String errorMessage) {
        showMessage(errorMessage);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onData(Cursor data) {
        adapter.changeCursor(data);
        switch(emptyViewSwitcher.getCurrentView().getId()) {
            case R.id.article_list_empty:
                if(adapter.getItemCount() > 0) {
                    emptyViewSwitcher.showNext();// Show article list
                }
                break;
            case R.id.article_list:
                if(adapter.getItemCount() == 0) {
                    emptyViewSwitcher.showPrevious();// Show the "no content" view
                }
                break;
        }
        swipeRefreshLayout.setRefreshing(false);
    }
}
