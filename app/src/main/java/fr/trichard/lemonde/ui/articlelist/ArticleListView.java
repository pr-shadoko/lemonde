package fr.trichard.lemonde.ui.articlelist;

import android.database.Cursor;
import android.support.annotation.StringRes;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 *
 */

public interface ArticleListView extends MvpView {
    void onError(String errorMessage);

    void onError(@StringRes int errorMessageResId);

    void onData(Cursor data);
}
