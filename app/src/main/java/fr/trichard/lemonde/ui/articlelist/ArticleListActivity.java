package fr.trichard.lemonde.ui.articlelist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import fr.trichard.lemonde.R;
import fr.trichard.lemonde.ui.CursorRecyclerViewAdapter;
import fr.trichard.lemonde.ui.ShareFloatingActionButton;
import fr.trichard.lemonde.ui.articledetail.ArticleDetailActivity;
import fr.trichard.lemonde.ui.articledetail.ArticleDetailFragment;

/**
 *
 */
public class ArticleListActivity extends AppCompatActivity implements CursorRecyclerViewAdapter.ArticleClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(savedInstanceState == null) {
            ArticleListFragment articleListFragment = new ArticleListFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.add(R.id.article_list_container, articleListFragment);
            transaction.commit();
        } else { // configuration change
            ArticleDetailFragment detailFragment = (ArticleDetailFragment) getSupportFragmentManager().findFragmentById(R.id.article_detail_container);
            long id = savedInstanceState.getLong(ArticleDetailFragment.ARG_ITEM_ID);
            if(getResources().getBoolean(R.bool.dual_pane)) { // dual-pane
                if(id != RecyclerView.NO_ID) { // article selected
                    // show the article detail alongside
                    onArticleClicked(id);
                }
                // else no article selected : nothing to do
            } else { // single-pane
                if(detailFragment != null) { // coming from dual-pane with article selected
                    // show the article detail instead
                    onArticleClicked(id);
                }
                // else coming from single pane landscape or no article was selected : nothing to do
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save currently selected article. The fragment is attached after Activity.onCreate() so at this time listFragment.getSelectedArticleId() returns -1
        ArticleListFragment listFragment = (ArticleListFragment) getSupportFragmentManager().findFragmentById(R.id.article_list_container);
        outState.putLong(ArticleDetailFragment.ARG_ITEM_ID, listFragment.getSelectedArticleId());
    }

    @Override
    public void onArticleClicked(long articleId) {
        if(getResources().getBoolean(R.bool.dual_pane)) {
            Bundle arguments = new Bundle();
            arguments.putLong(ArticleDetailFragment.ARG_ITEM_ID, articleId);
            ArticleDetailFragment fragment = new ArticleDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
            .replace(R.id.article_detail_container, fragment)
            .commit();
        } else {
            Intent intent = new Intent(this, ArticleDetailActivity.class);
            intent.putExtra(ArticleDetailFragment.ARG_ITEM_ID, articleId);
            startActivity(intent);
        }
    }
}
