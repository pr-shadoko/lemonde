package fr.trichard.lemonde.ui.articledetail;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import fr.trichard.lemonde.model.Article;
import fr.trichard.lemonde.model.ArticleCursorLoader;
import fr.trichard.lemonde.util.TaskCompletionListener;

/**
 *
 */

public class ArticleDetailPresenter extends MvpBasePresenter<ArticleDetailView> implements TaskCompletionListener<Article> {
    private ArticleCursorLoader articleCursorLoader;
    private Context context;

    public ArticleDetailPresenter(Context context) {
        this.context = context.getApplicationContext();
    }

    public void loadArticle(long articleId) {
        if(articleCursorLoader != null && articleCursorLoader.getStatus() == AsyncTask.Status.RUNNING) {
            return;
        }
        articleCursorLoader = new ArticleCursorLoader(context, this);
        articleCursorLoader.execute(articleId);
    }

    @Override
    public void onCompletion(Article article) {
        if(isViewAttached()) {
            //noinspection ConstantConditions
            getView().onData(article);
        }
    }
}